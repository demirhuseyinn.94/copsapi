import psycopg2
def getDatabaseList(serverIp, loginName, loginSecret):
    con = psycopg2.connect(database="postgres", user=loginName,
                           password=loginSecret, host=serverIp, port="5432")

    cur = con.cursor()
    cur.execute("SELECT datname FROM pg_database;")
    rows = cur.fetchall()
    dbList = []
    for row in rows:
        dbList.append(row[0])
    con.close()
    
    return dbList

def getDatabaseConfigurations(serverIp, loginName, loginSecret):
    con = psycopg2.connect(database="postgres", user=loginName,
                           password=loginSecret, host=serverIp, port="5432")

    cur = con.cursor()
    cur.execute('''SELECT name,
    setting
FROM pg_settings
WHERE name in ('shared_buffers','effective_cache_size','max_replication_slots') ''')
    rows = cur.fetchall()
    configurationList = []
    for row in rows:
        configurationList.append(row)
    con.close()
    return configurationList

def getDatabaseVersion(serverIp, loginName, loginSecret):
    con = psycopg2.connect(database="postgres", user=loginName,
                           password=loginSecret, host=serverIp, port="5432")

    cur = con.cursor()
    cur.execute(''' select version() ''')
    rows = cur.fetchone()
    con.close()
    return rows[0]

def createPostgresqlDatabase(serverIp, loginName, loginSecret,databaseName):
    con = psycopg2.connect(database="postgres", user=loginName,
                           password=loginSecret, host=serverIp, port="5432")
    con.autocommit = True
    databaseScript=f'''create database {databaseName} '''
    cur = con.cursor()
    cur.execute(databaseScript)
    con.commit()
    con.close()
    return "created"