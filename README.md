# Project Purpose and Scope 

The purpose of this project is to provide the necessary backend service for administration tasks on couchbase/postgresql to run as a service.

In project scope,

1. User and role management for Couchbase
2. Bucket management for Couchbase
3. Cluster management for PostgreSQL
4. Database Management for PostgreSQL

operations will be provided.

An integrated service will be provided where users can perform the administration tasks they need for user, role and bucket management as a service.

# Project Stack

| Programming Language | Python 3.9       |
| -------------------- | ---------------- |
| Framework            | Flask            |
| Database             | PostgreSQL       |
| Hosting              | Docker Container |


# Testing and Building

1. Clone the repository

2. Build and run docker application

```bash
docker build -t copsapi:v2 .
docker run -dit --rm -p 5000:5000 --name copsapi copsapi:v2
```


# References

## Couchbase

- https://docs.couchbase.com/server/current/manage/manage-security/manage-users-and-roles.html#manage-users-with-the-rest-api
- https://docs.couchbase.com/server/current/rest-api/rest-bucket-create.html


## Python

- https://docs.python.org/3/library/json.html#json.dumps
- https://pythonbasics.org/flask-http-methods/


