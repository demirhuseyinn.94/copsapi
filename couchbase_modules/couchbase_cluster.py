import requests


def getCouchbaseClusterDetails(serverIp,loginName,loginSecret):
    try:
        urlForHealth = f"http://{serverIp}:8091/pools/default"
        getClusterDetails = requests.get(
            url=urlForHealth, auth=(loginName, loginSecret))
        resultParsed = getClusterDetails.json()
        return resultParsed
    except Exception as couchbaseBucketException:
        return couchbaseBucketException

def getRebalanceDetails (serverIp,loginName,loginSecret):
    try:
        urlForRebalance = f"http://{serverIp}:8091/pools/default/rebalanceProgress"
        getRebalance = requests.get(
            url=urlForRebalance, auth=(loginName, loginSecret))
        resultParsed = getRebalance.json()
        return resultParsed
    except Exception as couchbaseRebalanceException:
        return couchbaseRebalanceException