import requests

def getCocuhbaseServerHealth(serverIp,loginName,loginSecret):
    """
    This function is responsible for getting the status of couchbase node
    """
    try:
        urlForHealth = f"http://{serverIp}:8091/pools/default"
        getNodeDetails = requests.get(
            url=urlForHealth, auth=(loginName, loginSecret))
        resultParsed = getNodeDetails.json()
        nodeStatus = "not reached"
        for node in resultParsed['nodes']:
            if node.get('hostname') == f"{serverIp}:8091":
                nodeStatus = node
        return nodeStatus
    except Exception as couchbaseHealthException:
        return False

def getCouchbaseUsers(serverIp,loginName,loginSecret):
    """
    This function is responsible for getting the users in a couchbase cluster
    """
    try:
        urlForHealth = f"http://{serverIp}:8091/settings/rbac/users"
        getNodeDetails = requests.get(
            url=urlForHealth, auth=(loginName, loginSecret))
        resultParsed = getNodeDetails.json()
        return resultParsed
    except Exception as couchbaseHealthException:
        return couchbaseHealthException

def createCouchbaseUser(serverIp,loginName,loginSecret,login_name,role_name):
    """
    This function is responsible for creating a new user in a couchbase cluster
    """
    try:
        urlForHealth = f"http://{serverIp}:8091/settings/rbac/users/local/{login_name}"
        getNodeDetails = requests.put(
            url=urlForHealth, auth=(loginName, loginSecret), data={'roles': role_name,'password':'changeme'})
        resultParsed = getNodeDetails.json()
        return resultParsed
    except Exception as couchbaseHealthException:
        return couchbaseHealthException