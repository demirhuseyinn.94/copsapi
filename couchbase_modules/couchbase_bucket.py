import requests


def createCouchbaseBucket(serverIp, loginName, loginSecret, bucketName, bucketType, bucketMemoryMB, numReplica, durabilityMinLevel):
    """
    This function is responsible for creating a new bucket in a couchbase cluster
    """
    try:
        urlForHealth = f"http://{serverIp}:8091/pools/default/buckets"
        postBucketUrl = requests.post(
            url=urlForHealth, auth=(loginName, loginSecret), data={'name': bucketName, 'bucketType': bucketType, 'ramQuotaMB': bucketMemoryMB, 'replicaNumber': numReplica, 'durabilityMinLevel': durabilityMinLevel})
        return postBucketUrl.status_code
    except Exception as couchbaseHealthException:
        return "Error Occured",500

def getCouchbaseBuckets(serverIp,loginName,loginSecret):
    try:
        urlForHealth = f"http://{serverIp}:8091/pools/default/buckets"
        getNodeDetails = requests.get(
            url=urlForHealth, auth=(loginName, loginSecret))
        resultParsed = getNodeDetails.json()
        bucketList=[]
        for bucket in resultParsed:
            bucketList.append(bucket.get('name'))
        return resultParsed
    except Exception as couchbaseBucketException:
        return couchbaseBucketException

def getCouchbaseBucketStats(serverIp,bucketName,loginName,loginSecret):
    try:
        urlForHealth = f"http://{serverIp}:8091/pools/default/buckets/{bucketName}/stats"
        getNodeDetails = requests.get(
            url=urlForHealth, auth=(loginName,loginSecret))
        resultParsed = getNodeDetails.json()
        return resultParsed
    except Exception as couchbaseBucketException:
        return couchbaseBucketException