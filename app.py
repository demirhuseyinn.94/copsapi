# ty stuffs
import json
from couchbase_modules import couchbase_role as couchbaseRole
from couchbase_modules import couchbase_bucket as couchbaseBucketRole
from couchbase_modules import couchbase_cluster as couchbaseClusterRole
from postgresql_modules import postgresql_db as postgresqlDatabase
# python packages
from flask import Flask, render_template, request
from flask_swagger_ui import get_swaggerui_blueprint
from flask_cors import CORS
import requests


app = Flask(__name__)


@app.route('/')
def index():
    f = open('./resources/swagger.json',)
    data = json.load(f)
    return data



@app.route('/databases', methods=['GET', 'POST'])
def databases():
    if request.method == 'GET':
        data = json.loads(request.data)
        server_information = data.get('server_ip')
        login_information = data.get('login_name')
        login_key = data.get('login_password')
        # get postgresql db information
        postgresqlConfigurations=postgresqlDatabase.getDatabaseConfigurations(server_information,login_information,login_key)
        postgresqlDatabases=postgresqlDatabase.getDatabaseList(server_information,login_information,login_key)
        postgresqlVersion=postgresqlDatabase.getDatabaseVersion(server_information,login_information,login_key)
        postgresqlDataModel={
            "databaseVersion": postgresqlVersion,
            "databaseConfigurations":postgresqlConfigurations,
            "databaseList" : postgresqlDatabases
        }
        return postgresqlDataModel

    else:
        try:

            data = json.loads(request.data)
            server_information = data.get('server_ip')
            login_information = data.get('login_name')
            login_key = data.get('login_password')
            new_database_name = data.get('new_database_name')
            postgresqlDatabase.createPostgresqlDatabase(server_information,login_information,login_key,new_database_name)
            return {"message": "Created"}, 200
        except Exception as createNewUserException:
            return {"message": createNewUserException}, 500

@app.route('/users', methods=['GET', 'POST'])
def users():
    if request.method == 'GET':
        data = json.loads(request.data)
        server_information = data.get('server_ip')
        login_information = data.get('login_name')
        login_key = data.get('login_password')
        node_status = couchbaseRole.getCocuhbaseServerHealth(
            server_information, login_information, login_key)
        node_health = node_status.get('status')
        if node_health == 'healthy':
            couchbase_users = couchbaseRole.getCouchbaseUsers(
                server_information, login_information, login_key)
            couchbase_jsonify_user = json.dumps(
                couchbase_users, indent=4, sort_keys=True)
            return couchbase_jsonify_user, 200
        else:
            return {"message": "Error Occured"}, 500
    else:
        try:

            data = json.loads(request.data)
            server_information = data.get('server_ip')
            login_information = data.get('login_name')
            login_key = data.get('login_password')
            new_login_name = data.get('new_login_name')
            role_name = data.get('role_name')
            couchbaseRole.createCouchbaseUser(
                server_information, login_information, login_key, new_login_name, role_name)
            return {"message": "Created"}, 200
        except Exception as createNewUserException:
            return {"message": createNewUserException}, 500


@app.route('/bucket', methods=['GET', 'POST'])
def bucket():
    if request.method == 'GET':
        data = json.loads(request.data)
        server_information = data.get('server_ip')
        login_information = data.get('login_name')
        login_key = data.get('login_password')
        node_status = couchbaseRole.getCocuhbaseServerHealth(
            server_information, login_information, login_key)
        node_health = node_status.get('status')
        if node_health == 'healthy':
            couchbase_buckets = couchbaseBucketRole.getCouchbaseBuckets(
                server_information, login_information, login_key)
            bucketList=[]
            for bucket in couchbase_buckets:
                bucketName = bucket.get('name')
                bucketStats = bucket.get('basicStats')
                bucketResidentRatio = couchbaseBucketRole.getCouchbaseBucketStats(
                    server_information, bucketName, login_information, login_key)
                avgResident = sum(bucketResidentRatio['op']['samples']['vb_active_resident_items_ratio'])/len(
                    bucketResidentRatio['op']['samples']['vb_active_resident_items_ratio'])
                bucketRecord = {
                    "bucketName": bucketName,
                    "bucketType": bucket.get('bucketType'),
                    "bucketReplicas": bucket.get('vBucketServerMap').get('numReplicas'),
                    "bucketQuotaPercentage": round(bucketStats.get('quotaPercentUsed'), 1),
                    "bucketItemCount":  round(bucketStats.get('itemCount')),
                    "bucketResidentRatio": avgResident,
                    "bucketDisUsedInMb": round((bucketStats.get("diskUsed"))/1024/1024, 1)
                }
                bucketList.append(bucketRecord)
            couchbase_jsonify_buckets = json.dumps(
                bucketList, indent=4, sort_keys=True)
            return couchbase_jsonify_buckets, 200
        else:
            return {"message": "Error Occured"}, 500
    else:
        try:

            data = json.loads(request.data)
            server_information = data.get('server_ip')
            login_information = data.get('login_name')
            login_key = data.get('login_password')
            bucketName = data.get('bucket_name')
            bucketType = data.get('bucket_type')
            bucketMemoryMB = data.get('memory_limit')
            numReplica = data.get('num_replica')
            durabilityMinLevel=data.get('durability_level')
            bucketResult=couchbaseBucketRole.createCouchbaseBucket(
                server_information, login_information, login_key,bucketName, bucketType, bucketMemoryMB, numReplica, durabilityMinLevel)
            print(bucketResult)
            return "finished", bucketResult
        except Exception as createNewBucketException:
            return {"message": createNewBucketException}, 500



@app.route('/cluster', methods=['GET'])
def cluster():
    if request.method == 'GET':
        data = json.loads(request.data)
        server_information = data.get('server_ip')
        login_information = data.get('login_name')
        login_key = data.get('login_password')
        node_status = couchbaseRole.getCocuhbaseServerHealth(
            server_information, login_information, login_key)
        node_health = node_status.get('status')
        if node_health == 'healthy':
            couchbase_cluster = couchbaseClusterRole.getCouchbaseClusterDetails(
                server_information, login_information, login_key)
            couchbase_buckets = couchbaseBucketRole.getCouchbaseBuckets(
                server_information, login_information, login_key)
            couchbase_rebalance=couchbaseClusterRole.getRebalanceDetails(server_information, login_information, login_key)
            isRbRunning=couchbase_rebalance.get('status')
            bucketList=[]
            for bucket in couchbase_buckets:
                bucketName = bucket.get('name')
                bucketList.append(bucketName)
            memoryLimitData=couchbase_cluster.get('memoryQuota')
            memoryLimitIndex=couchbase_cluster.get('indexMemoryQuota')
            memoryLimitEventing=couchbase_cluster.get('eventingMemoryQuota')
            memoryLimitFts=couchbase_cluster.get('ftsMemoryQuota')
            memoryLimitSearch=couchbase_cluster.get('cbasMemoryQuota')
            couchbaseServices=[]
            totalNodeCount=0
            couchbaseNodes=couchbase_cluster.get('nodes')
            clusterVersion=couchbaseNodes[0].get('version')
            for node in couchbaseNodes:
                couchbaseServices.append(node.get('services'))
                totalNodeCount=totalNodeCount+1

            cluster_details={
                "clusterVersion":clusterVersion,
                "dataServiceMemoryLimit": memoryLimitData,
                "indexServiceMemoryLimit": memoryLimitIndex,
                "eventingServiceMemoryLimit": memoryLimitEventing,
                "ftsServiceMemoryLimit": memoryLimitFts,
                "analyticsServiceMemoryLimit": memoryLimitSearch,
                "runningRebalance" : isRbRunning,
                "clusterServices": couchbaseServices,
                "totalNodeCount" : totalNodeCount,
                "bucketList" : bucketList

            }
            cocuhase_jsonify_cluster = json.dumps(
                cluster_details, indent=4, sort_keys=False)
            return cocuhase_jsonify_cluster, 200
        else:
            return {"message": "Error Occured"}, 500
    else:
        return {"message": "Only GET request is supported"}, 500

if __name__ == '__main__':
    SWAGGER_URL = '/api/docs'
    API_URL = 'http://localhost:5000'

    swaggerui_blueprint = get_swaggerui_blueprint(
        SWAGGER_URL,
        API_URL,
        config={
            'app_name': "copsapi"
        },
    )
    CORS(app)
    app.register_blueprint(swaggerui_blueprint)
    app.run(host="0.0.0.0", port=5000)
